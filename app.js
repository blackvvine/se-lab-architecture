
// import model module
var model = require('./data');

// import app config
var config = require('./config');

var express = require('express');
var bodyParser = require("body-parser");

var app = express();

// former: StudentModel.remove({}, function (err) {} );
model.removeAllStudents();

// former:
// new StudentModel({ name: "Pardis", sirname: "Pasha", studentid: 91101234, gpa: 11.4}).save();
// new StudentModel({ name: "Mozhdeh", sirname: "Gheini", studentid: 91105464, gpa: 19.90}).save();
model.addStudent({ name: "Pardis", sirname: "Pasha", studentid: 91101234, gpa: 11.4});
model.addStudent({ name: "Mozhdeh", sirname: "Gheini", studentid: 91105464, gpa: 19.90});

app.use(express.static(process.cwd()+"/public"));
app.use(bodyParser.urlencoded( {extended: true} ));
app.use(bodyParser.json());

app.get('/students', function(req, res) {
    // former:
    // StudentModel.find({}, function(error, data) {
    //   if (data)
    //   res.json(data);
    model.getAllStudents(function(error, data) {
        if (data)
            res.json(data);
    });
});

app.get('/students/:studentid/delete', function(req, res) {
    // former:
    // StudentModel.remove({studentid: req.params.studentid}, function(error) {
    //     res.json({code: error!=null});
    // });
    model.removeStudent(req.params.studentid, function(error) {
        res.json({code: error!=null});
    });
});

app.post('/students/add', function(req, res) {
    // former:
    // new StudentModel(
    //         {name: req.body.name,
    //             sirname: req.body.sirname,
    //             studentid: req.body.studentid,
    //             gpa: req.body.gpa})
    //     .save( function(error) {
    //         res.json({code: error!=null});
    //     });
    model.addStudent(
            {
                name: req.body.name,
                sirname: req.body.sirname,
                studentid: req.body.studentid,
                gpa: req.body.gpa
            },
            function(error) {
                console.log(error);
                res.json({code: error!=null});
            });
});


app.post('/students/:studentid/update', function(req, res) {
    model.updateStudent(
            req.params.studentid,
            req.body,
            function(error) {
                console.log(error);
                res.json({code: error!=null});
            });
});


app.listen(config.server_port);


