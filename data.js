
// import app config
var config = require('./config');

// import MongoDB connection 
var mongoose = require('mongoose');


// create connection to MongoDB
mongoose.connect("mongodb://localhost:" + config.mongodb_port + "/");

// define model
var StudentStructure = {
    name: String,
    sirname: String,
    studentid: Number,
    gpa: Number
};

// create schema and model
var StudentSchema = new mongoose.Schema(StudentStructure);
var StudentModel = mongoose.model("Student", StudentSchema);

//module.exports = {
//    StudentModel
//};

function addStudent(student, callback) {
    if (typeof callback === 'undefined') {
        new StudentModel(student).save();
    } else {
        new StudentModel(student).save(callback);
    }
}

function removeAllStudents() {
    StudentModel.remove({}, function(err){});
}

function getAllStudents(callback) {
    StudentModel.find({}, callback);
}

function removeStudent(id, callback) {
    StudentModel.remove({studentid: id}, callback);
}

function updateStudent(id, info, callback) {
    StudentModel.find({_id: id}, function(err, res) {
        console.log(res[0]);
        var st = res[0];
        for (f in info) {
            st[f] = info[f];
        }
        st.save(callback);
    });
}

module.exports = {
    addStudent, removeAllStudents, getAllStudents, removeStudent, updateStudent
}


